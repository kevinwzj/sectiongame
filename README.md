# Section Game

Your task is to build a web-service to create  apps  which have  games  associated with them
which are contained in  sections.


# Create a new app

Request url:
`POST https://180d7b3c.ngrok.io/api/section-game/apps`

Request body:
```json
{"app_name": "section game 2"}
```

Response:
```json
{
    "message": "App 'section game 2' is created/updated successfully",
    "url": {
        "Available games end-point": "GET /api/section-game/apps/4/games",
        "Turn on/off games end-point": "PUT /api/section-game/apps/4/games",
        "Sections and games endpoint": "GET /api/section-game/apps/4/sections-games"
    }
}
```

Note: 
1. 180d7b3c.ngrok.io is running from my computer. You can replace the domain by your own.
2. Copy .env.sample as .env

# Brief Introduction

Use Laravel 5.7 framework to build up the demo.

Database Migrations:
```
/database/migrations/
```

Models:
```
/app/Models/
```

Controllers:
```
/app/Http/Controllers/SectionGame/SectionGameController.php
/app/Http/Controllers/DataFeed/DataFeedController.php
```

API Route
```
/routes/api.php
```