<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::prefix('section-game')->group(function () {
    Route::namespace('SectionGame')->group(function () {
        // Controllers Within The "App\Http\Controllers\SectionGame" Namespace
        Route::get('apps/{app_id}/games', 'SectionGameController@indexGames');
        Route::put('apps/{app_id}/games', 'SectionGameController@updateGames');
        Route::get('apps/{app_id}/sections-games', 'SectionGameController@indexActiveSectionsGames');
        Route::post('apps', 'SectionGameController@store');
    });
});

Route::prefix('data-feed')->group(function () {
    Route::namespace('DataFeed')->group(function () {
        // Controllers Within The "App\Http\Controllers\DataFeed" Namespace
        Route::get('load', 'DataFeedController@load');
    });
});