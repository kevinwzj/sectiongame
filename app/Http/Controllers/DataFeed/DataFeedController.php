<?php

namespace App\Http\Controllers\DataFeed;

use App\Http\Controllers\Controller;

class DataFeedController extends Controller
{
    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function load()
    {
        $data = '{  
                   "sections":[  
                      {  
                         "key":"my_profile",
                         "label":"My Profile"
                      },
                      {  
                         "key":"my_network",
                         "label":"My Network"
                      },
                      {  
                         "key":"actions",
                         "label":"Actions"
                      }
                   ],
                   "games":[  
                      {  
                         "key":"explore_app",
                         "label":"Explore App",
                         "sections":[  
                            "my_profile",
                            "actions"
                         ]
                      },
                      {  
                         "key":"attendee_networking",
                         "label":"Attendee Networking",
                         "sections":[  
                            "my_network",
                            "actions"
                         ]
                      }
                   ]
                }';
        return response()->json(json_decode($data, true));
    }
}
