<?php

namespace App\Http\Controllers\SectionGame;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Route;
use App\Models\App;
use App\Models\Game;
use App\Models\Section;
use App\Models\SectionGame;

class SectionGameController extends Controller
{
    protected $app;
    protected $game;
    protected $section;
    protected $sectionGame;

    public function __construct()
    {
        $this->app = new App();
        $this->game = new Game();
        $this->section = new Section();
        $this->sectionGame = new SectionGame();
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        $app = json_decode($request->getContent(), true);
        $appName = $app['app_name'];

        // load json data
        $request = Request::create('/api/data-feed/load', 'GET');
        $response = Route::dispatch($request);
        $appDetailData = json_decode($response->getContent(), true);

        // store app info
        $appData = [
            'name' => $appName
        ];
        $appId = $this->app->store($appData);

        // store games info
        $games = $appDetailData['games'];
        foreach ($games as $game) {
            $item = [];
            $item['key'] = $game['key'];
            $item['label'] = $game['label'];
            $item['status'] = 1;
            $item['app_id'] = $appId;
            $this->game->store($item);
        }

        // store sections info
        $sections = $appDetailData['sections'];
        foreach ($sections as $section) {
            $item = [];
            $item['key'] = $section['key'];
            $item['label'] = $section['label'];
            $item['app_id'] = $appId;
            $this->section->store($item);
        }

        // store game-section relationship info
        foreach($games as $game) {
            foreach($game['sections'] as $key) {
                $item = [];
                $sectionId = $this->section->getIdByKey($key, $appId);
                $gameId = $this->game->getIdByKey($game['key'], $appId);
                $item['section_id'] = $sectionId;
                $item['game_id'] = $gameId;
                $this->sectionGame->store($item);
            }
        }

        return response()->json([
            'message' => "App '{$appName}' is created/updated successfully",
            'url' => [
                'Available games end-point' => "GET /api/section-game/apps/{$appId}/games",
                'Turn on/off games end-point' => "PUT /api/section-game/apps/{$appId}/games",
                'Sections and games endpoint' => "GET /api/section-game/apps/{$appId}/sections-games"
            ]
        ]);
    }

    /**
     * @param $app_id
     * @return \Illuminate\Http\JsonResponse
     */
    public function indexGames($app_id)
    {
        try {
            $games = $this->app->find($app_id)->games;
        } catch(\Exception $exception) {
            return response()->json([
                'message' => 'App id does not exist'
            ]);
        }
        $data = [];
        foreach ($games as $game) {
            $data[] = [
                'key' => $game->key,
                'label' => $game->label
            ];
        }
        $result= [
            'data' => [
                'games' => $data
            ]
        ];
        return response()->json($result);
    }

    /**
     * @param Request $request
     * @param $app_id
     * @return \Illuminate\Http\JsonResponse
     */
    public function updateGames(Request $request, $app_id)
    {
        $games = json_decode($request->getContent(),true);
        foreach ($games as $key => $status) {
            $item = [];
            $item['app_id'] = $app_id;
            $item['key'] = $key;
            $item['status'] = $status;
            $this->game->updateByKeyAndAppId($item);
        }
        return response()->json([
            'message' => 'Games status updated'
        ]);
    }

    /**
     * @param $app_id
     * @return \Illuminate\Http\JsonResponse
     */
    public function indexActiveSectionsGames($app_id)
    {
        $collection = $this->sectionGame->getActiveSectionsGames($app_id);
        $sections = [];
        foreach ($collection as $item) {
            $sections[$item['section_key']] = $item['section_label'];
        }
        $result = [];
        foreach ($sections as $sectionKey => $sectionLabel) {
            $games = [];
            foreach ($collection as $item) {
                if ($item->section_key == $sectionKey) {
                    $games[] = [
                        'key' => $item->game_key,
                        'label' => $item->game_label
                    ];
                }
            }
            $result[] = [
                'key' => $sectionKey,
                'label' => $sectionLabel,
                'games' => $games
            ];
        }
        return response()->json($result);
    }
}
