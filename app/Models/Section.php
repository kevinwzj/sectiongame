<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Section extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'sections';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'key', 'label', 'app_id'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function app()
    {
        return $this->belongsTo('App\Models\App','app_id');
    }

    /**
     * @param array $data
     */
    public function store(array $data)
    {
        $this->firstOrCreate($data);
    }

    /**
     * @param $key
     * @param $appId
     * @return mixed
     */
    public function getIdByKey($key, $appId)
    {
        $section = $this->where('key', $key)->where('app_id', $appId)->first();
        return $section->id;
    }
}
