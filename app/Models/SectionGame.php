<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SectionGame extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'section_games';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'section_id', 'game_id'
    ];

    /**
     * @param array $data
     */
    public function store(array $data)
    {
        $this->firstOrCreate($data);
    }

    /**
     * @param $app_id
     * @return mixed
     */
    public function getActiveSectionsGames($app_id)
    {
        $collection = $this->select('sections.key as section_key', 'sections.label as section_label', 'games.key as game_key', 'games.label as game_label')
            ->leftjoin('sections', 'sections.id', '=', 'section_games.section_id')
            ->leftjoin('games', 'games.id', '=', 'section_games.game_id')
            ->where('sections.app_id', $app_id)
            ->where('games.status', 1)
            ->get();
        return $collection;
    }
}
